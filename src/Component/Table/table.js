import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import { Formik, Field, Form } from "formik";
import "./Table.css"

function Table(props) {
    const togglechange=(item,key)=>{
        var data2=props.data
        data2[key].toggle=!item.toggle
        props.setdata(data2);
        props.setToggle(!props.toggle)
      }
      const edittext=(item,key)=>{
        var data2=props.data
        data2[key].edit=!item.edit
        props.setdata(data2);
        props.setToggle(!props.toggle)
      }
      const saveText=(item,key,values)=>{
        var data2=props.data
        data2[key].name=values.name
        data2[key].edit=!item.edit
        props.setdata(data2);
        props.setToggle(!props.toggle)
      }
      const deletdata=(item)=>{
        var data2=props.data.filter(function(item2) {
          return item !== item2;
      });
      props.setdata(data2);
      props.setToggle(!props.toggle)
      }
    return (
        props.data.map((item,key)=>{
            if(item.toggle===props.toggle){
            return(
              <div>
             <Checkbox defaultChecked={item.toggle} onChange={()=>togglechange(item,key)}inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} />
             {!item.edit?<span>{item.name}</span>:null}
             {item.edit?<div>
                <Formik
        initialValues={{ name:item.name }}
        onSubmit={async values => {
        //   await new Promise(resolve => setTimeout(resolve, 500));
        //   alert(JSON.stringify(values, null, 2));
        saveText(item,key,values)
        }}
      >
        <Form>
        {/* <TextField id="outlined-basic" label="item name"  name="name" defaultValue={item.name} id={key} variant="outlined" /> */}
          <Field name="name" type="text" />
          <Button style={{marginLeft:"50px"}} variant="outlined" type="submit">Save</Button>
        </Form>
      </Formik>
             </div>:null}
         {/* {!item.edit?<span>{item.name}</span>:<TextField id="outlined-basic" label="item name" defaultValue={item.name} id={key} variant="outlined" />} */}
         {!item.edit?<Button style={{marginLeft:"50px"}} variant="outlined" onClick={()=>edittext(item,key)}>Edit</Button>:null}
         <Button style={{marginLeft:"50px"}} variant="outlined" 
         onClick={()=>deletdata(item)}>Delete</Button>
         </div>
            );}
          })
    );
  }
  
  export default Table;