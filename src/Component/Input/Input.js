// import './App.css';
import Button from '@material-ui/core/Button';
import { Formik, Field, Form } from "formik";


function Text(props) {
    return (
      <div >
         <Formik
        initialValues={{ name:"" }}
        onSubmit={async values => {
          // await new Promise(resolve => setTimeout(resolve, 500));
        props.Add(values)
        }}
      >
        <Form>
          <Field name="name" type="text" />
          <Button style={{marginLeft:"50px"}} variant="outlined" type="submit">Add</Button>
        </Form>
      </Formik>
       </div>
    );
  }
  
  export default Text;
  